0: Successful + user close
1: Restarting for update
2: 'exitOnLaunch' exit
3: TS3 running and user exits
4: User not an admin
5: Couldn't connect to server
6: User exits in file chooser
7: User exits during install
8: User exits in DlgFiles
9: Connection to server was lost
10: Not enough space in WORK_DIR drive
11: Not enough space in arma path drive
12: User exits in DlgLanguage
13: Arma 3 running and user exits
-1: Debug exit
