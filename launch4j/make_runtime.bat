@echo off
set cdir=%~dp0
set jre=%cdir%/BUILDS/jre

@REM if dependencies change:
@REM jdeps -s "%cdir%/../out/MCo_Tool.jar

rd /s /q "%jre%"
jlink -G --no-header-files --no-man-pages --add-modules java.base,java.desktop,java.logging --output "%jre%"

rcedit "%jre%/bin/java.exe" --set-version-string "FileDescription" "MCo Tool Runtime"
rcedit "%jre%/bin/javaw.exe" --set-version-string "FileDescription" "MCo Tool Runtime"

rcedit "%jre%/bin/java.exe" --set-icon "%cdir%/BUILDS/icon.ico"
rcedit "%jre%/bin/javaw.exe" --set-icon "%cdir%/BUILDS/icon.ico"

pause
