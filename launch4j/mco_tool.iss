[Setup]
AppId="567687fa-4342-43b8-ba9e-161808421667"
AppName="MCo Tool"
AppVerName="MCo Tool"
AppVersion="2.5.6.0 (R61)"
VersionInfoVersion="2.5.6.0"
AppCopyright="Copyright © 2015-2023 Ozan Egitmen"

DisableProgramGroupPage=yes
DefaultDirName="{localappdata}\MCo Tool"
UninstallDisplayIcon="{app}\MCo Tool.exe"

Compression="lzma2/max"
SolidCompression=yes

WizardStyle="modern"
WizardImageFile="WizardImageFile.bmp"
WizardSmallImageFile="WizardSmallImageFile.bmp"
SetupIconFile="setup.ico"
OutputDir="."
OutputBaseFilename="MCo Tool Setup"

[Tasks]
Name: "desktopicon"; Description: "Create a Desktop icon";

[Files]
Source: "BUILDS\jre\*"; DestDir: "{app}\jre"; Flags: recursesubdirs
Source: "BUILDS\LICENCE.html"; DestDir: "{app}"
Source: "BUILDS\MCo Tool Force Update.bat"; DestDir: "{app}"
Source: "BUILDS\MCo Tool Logger.bat"; DestDir: "{app}"
Source: "BUILDS\MCo Tool.exe"; DestDir: "{app}"
Source: "BUILDS\README.txt"; DestDir: "{app}"

[Icons]
Name: "{userdesktop}\MCo Tool"; Filename: "{app}\MCo Tool.exe"; Tasks: desktopicon

[Run]
Filename: "{app}\MCo Tool.exe"; Description: "Launch MCo Tool"; Flags: postinstall shellexec

[UninstallDelete]
Type: files; Name: "{app}\database.keys"
Type: files; Name: "{app}\$bannercache.png"
Type: files; Name: "{app}\7za.exe"
Type: files; Name: "{app}\ext_asst.exe"
