/*
 * Copyright (C) 2015-2023 Ozan Egitmen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.mcotool.client.gui.dialogs;

import java.awt.*;
import javax.swing.*;

import static com.mcotool.client.Main.*;
import static javax.swing.SwingUtilities.isLeftMouseButton;

@SuppressWarnings("ConstantConditions")
public class DlgLanguage extends JDialog {

    public String lang = "";
    private Point initialClick;

    public DlgLanguage(Frame parent) {
        super(parent, true);
        getRootPane().setBorder(UI_BORDER);
        initComponents();
        titleBar.setFont(font(2, 16));
    }

    private void setLang(String lang) {
        this.lang = lang;
        setLanguage(lang);
        setVisible(false);
    }

    private void initComponents() {
        btnExit = new com.mcotool.client.TLabel();
        titleBar = new JLabel();
        lblTurkish = new JLabel();
        lblEnglish = new JLabel();
        lblFinnish = new JLabel();

        setIconImage(new ImageIcon(getClass().getResource("/com/mcotool/client/rsc/img/icon.png")).getImage());
        setUndecorated(true);
        setResizable(false);

        btnExit.setBackground(UI_TONE);
        btnExit.setHorizontalAlignment(SwingConstants.CENTER);
        btnExit.setIcon(new ImageIcon(getClass().getResource("/com/mcotool/client/rsc/toolbar/x.png")));
        btnExit.setOpaque(true);
        btnExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnExitMouseClicked(evt);
            }

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnExitMouseEntered();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnExitMouseExited();
            }

            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnExitMousePressed(evt);
            }
        });

        titleBar.setBackground(UI_TONE);
        titleBar.setIcon(new ImageIcon(new ImageIcon(getClass().getResource("/com/mcotool/client/rsc/img/icon.png")).getImage().getScaledInstance(22, 22, Image.SCALE_SMOOTH)));
        titleBar.setText("Language / Dil / Kieli");
        titleBar.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        titleBar.setOpaque(true);
        titleBar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                titleBarMouseDragged(evt);
            }
        });
        titleBar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                titleBarMousePressed(evt);
            }
        });

        lblTurkish.setIcon(new ImageIcon(getClass().getResource("/com/mcotool/client/rsc/img/lang/turkish.png")));
        lblTurkish.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblTurkishMouseClicked(evt);
            }

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblTurkishMouseEntered();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblTurkishMouseExited();
            }
        });

        lblEnglish.setIcon(new ImageIcon(getClass().getResource("/com/mcotool/client/rsc/img/lang/english.png")));
        lblEnglish.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblEnglishMouseClicked(evt);
            }

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblEnglishMouseEntered();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblEnglishMouseExited();
            }
        });

        lblFinnish.setIcon(new ImageIcon(getClass().getResource("/com/mcotool/client/rsc/img/lang/finnish.png")));
        lblFinnish.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblFinnishMouseClicked(evt);
            }

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblFinnishMouseEntered();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblFinnishMouseExited();
            }
        });

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(titleBar, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(btnExit, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                        .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(lblFinnish)
                                        .addComponent(lblTurkish)
                                        .addComponent(lblEnglish))
                                .addGap(40, 40, 40))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                        .addComponent(titleBar, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnExit, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                                .addGap(20, 20, 20)
                                .addComponent(lblEnglish)
                                .addGap(20, 20, 20)
                                .addComponent(lblTurkish)
                                .addGap(20, 20, 20)
                                .addComponent(lblFinnish)
                                .addGap(20, 20, 20))
        );

        pack();
    }

    private void btnExitMouseClicked(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            setVisible(false);
        }
    }

    private void btnExitMouseEntered() {
        btnExit.setBackground(Color.RED);
    }

    private void btnExitMouseExited() {
        btnExit.setBackground(UI_TONE);
    }

    private void titleBarMouseDragged(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            int X = getLocation().x;
            int Y = getLocation().y;
            setLocation(X + (X + evt.getX()) - (X + initialClick.x), Y + (Y + evt.getY()) - (Y + initialClick.y));
        }
    }

    private void titleBarMousePressed(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            initialClick = evt.getPoint();
        }
    }

    private void btnExitMousePressed(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            btnExit.setBackground(new Color(255, 64, 64));
        }
    }

    private void lblEnglishMouseEntered() {
        lblEnglish.setIcon(getImageIcon("img/lang/englishOver"));
    }

    private void lblEnglishMouseExited() {
        lblEnglish.setIcon(getImageIcon("img/lang/english"));
    }

    private void lblTurkishMouseEntered() {
        lblTurkish.setIcon(getImageIcon("img/lang/turkishOver"));
    }

    private void lblTurkishMouseExited() {
        lblTurkish.setIcon(getImageIcon("img/lang/turkish"));
    }

    private void lblFinnishMouseEntered() {
        lblFinnish.setIcon(getImageIcon("img/lang/finnishOver"));
    }

    private void lblFinnishMouseExited() {
        lblFinnish.setIcon(getImageIcon("img/lang/finnish"));
    }

    private void lblEnglishMouseClicked(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            setLang("english");
        }
    }

    private void lblTurkishMouseClicked(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            setLang("turkish");
        }
    }

    private void lblFinnishMouseClicked(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            setLang("finnish");
        }
    }

    private com.mcotool.client.TLabel btnExit;
    private JLabel lblEnglish;
    private JLabel lblFinnish;
    private JLabel lblTurkish;
    private JLabel titleBar;
}
