/*
 * Copyright (C) 2015-2023 Ozan Egitmen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.mcotool.client.gui.dialogs;

import com.mcotool.client.Database;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

import static com.mcotool.client.Main.*;
import static javax.swing.SwingUtilities.isLeftMouseButton;

@SuppressWarnings("ConstantConditions")
public class DlgLaunch extends JDialog {

    private Point initialClick;
    private boolean btnLaunchEnabled = true;
    private final ArrayList<String> launchMods = new ArrayList<>();
    private final JPanel panelTop = new JPanel(), panelBot = new JPanel();
    private Object[][] svData, modData;
    private final java.util.Timer isArmaRunningTimer = new java.util.Timer();

    public DlgLaunch(Frame parent, ArrayList<String> mods) {
        super(parent, true);
        getRootPane().setBorder(UI_BORDER);
        initComponents();
        for (Object[] oa : new Object[][]{
                {titleBar, 2, 16},
                {lblOptionals, 1, 17},
                {lblServers, 1, 17},
                {btnLaunch, 0, 20},
                {btnInstallOptionals, 0, 20}
        }) {
            ((Component) oa[0]).setFont(font(oa[1], (int) oa[2]));
        }
        launchMods.addAll(mods.stream().filter(mod -> !mod.contains("userconfig/")).toList());
        for (JScrollPane pane : new JScrollPane[]{jScrollPane1, jScrollPane2}) {
            pane.getVerticalScrollBar().setUI(new javax.swing.plaf.basic.BasicScrollBarUI() {

                @Override
                protected JButton createDecreaseButton(int orientation) {
                    JButton b = new JButton();
                    b.setPreferredSize(new Dimension(0, 0));
                    return b;
                }

                @Override
                protected JButton createIncreaseButton(int orientation) {
                    JButton b = new JButton();
                    b.setPreferredSize(new Dimension(0, 0));
                    return b;
                }
            });
            pane.getVerticalScrollBar().setUnitIncrement(15);
        }
        panelTop.setLayout(new BoxLayout(panelTop, BoxLayout.Y_AXIS));
        panelBot.setLayout(new BoxLayout(panelBot, BoxLayout.Y_AXIS));
        isArmaRunningTimer.schedule(new java.util.TimerTask() {
            private boolean lastCycle = false;

            @Override
            public void run() {
                boolean isArmaRunning = isProcessRunning("arma3");

                if (isArmaRunning == lastCycle) {
                    return;
                }
                lastCycle = isArmaRunning;

                btnLaunchEnabled = !isArmaRunning;
                btnLaunch.setToolTipText(isArmaRunning ? localize("a3_already_running") : null);
                btnLaunch.setBackground(isArmaRunning ? Color.GRAY : UI_TONE);
            }
        }, 0, GLOBAL_SCHEDULER_TIME);
        btnInstallOptionals.setBackground(Color.GRAY);
        btnInstallOptionals.setToolTipText(localize("feature_not_yet_supported"));
        new Thread(this::populateModList).start();
        new Thread(this::populateServerList).start();
    }

    @SuppressWarnings("ConstantConditions")
    private void populateModList() {
        String path = Database.get("path", "");
        File workshopFolder = new File(path + "/!Workshop");
        ArrayList<String> modFolders = new ArrayList<>(java.util.Arrays.asList(new File(path).list()));
        if (workshopFolder.exists()) {
            for (String workshopMod : workshopFolder.list()) {
                modFolders.add("!Workshop/" + workshopMod);
            }
        }
        for (int i = 0; i < modFolders.size(); i++) {
            String modFolder = toLower(modFolders.get(i).replace('\\', '/'));
            if (modFolder.contains("@") && !launchMods.contains(modFolder.substring(modFolder.indexOf('@')))) {
                modFolders.set(i, path + "/" + modFolder);
            } else {
                modFolders.set(i, "::");
            }
        }
        modFolders.removeAll(java.util.Collections.singleton("::"));
        modData = new Object[modFolders.size()][2];
        for (int i = 0; i < modFolders.size(); i++) {
            String mod = modFolders.get(i);
            String displayName = mod.substring(mod.indexOf('@'));
            File cpp = new File(mod + "/mod.cpp");
            if (cpp.exists()) {
                try (BufferedReader br = new BufferedReader(new FileReader(cpp))) {
                    String line;
                    while ((line = br.readLine()) != null) {
                        if (line.contains("name")) {
                            if (line.indexOf('\"') < line.lastIndexOf('\"')) {
                                displayName = line.substring(line.indexOf('\"') + 1, line.lastIndexOf('\"'));
                            } else {
                                displayName = line.substring(line.indexOf('=') + 1, line.lastIndexOf(';'));
                            }
                            break;
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
            int length = displayName.length();
            if (mod.contains("!")) {
                displayName = "<b>(Workshop)</b> ".concat(displayName);
                length = displayName.length() - 7; // Remove the bold tags from length calculation
            }
            if (length > 45) {
                int substringEndIndex = 42;

                if (mod.contains("!")) {
                    // Add 7 more chars for the bold tags
                    substringEndIndex += 7;
                }

                displayName = displayName.substring(0, substringEndIndex) + "...";
            }
            displayName = "<html>".concat(displayName).concat("</html>");
            final int index = i;
            JLabel label = new JLabel(displayName, getImageIcon("toolbar/cb-"), SwingConstants.LEADING);
            label.setFont(new Font("Consolas", Font.PLAIN, 16));
            label.setMaximumSize(new Dimension(448, 25));
            label.setBorder(new javax.swing.border.EmptyBorder(3, 3, 3, 0));
            label.addMouseListener(new java.awt.event.MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent evt) {
                    if (!isLeftMouseButton(evt)) {
                        return;
                    }
                    modData[index][0] = !(boolean) modData[index][0];
                    if ((boolean) modData[index][0]) {
                        Database.setBool((String) modData[index][1], true);
                    } else {
                        Database.set((String) modData[index][1], null);
                    }
                    mouseEntered(evt);
                }

                @Override
                public void mouseEntered(MouseEvent evt) {
                    handleMouse(evt, true);
                }

                @Override
                public void mouseExited(MouseEvent evt) {
                    handleMouse(evt, false);
                }

                private void handleMouse(MouseEvent evt, boolean ent) {
                    JLabel source = (JLabel) evt.getSource();
                    source.setIcon(getImageIcon("toolbar/cb" + (((boolean) modData[index][0]) ? (ent ? "++" : "+") : (ent ? "--" : "-"))));
                    source.setForeground(ent ? new Color(200, 200, 200) : Color.WHITE);
                }
            });
            modData[i][0] = Database.getBool(mod.substring(path.length() + 1), false);
            modData[i][1] = mod.substring(path.length() + 1);
            label.setIcon(getImageIcon("toolbar/cb" + (((boolean) modData[i][0]) ? "+" : "-")));
            panelTop.add(label);
            jScrollPane1.setViewportView(panelTop);
        }
    }

    private void populateServerList() {
        ArrayList<String> servers = new ArrayList<>();
        java.net.HttpURLConnection serversConnection = url("servers.txt");
        try (BufferedReader br = new BufferedReader(new InputStreamReader(serversConnection.getInputStream(), StandardCharsets.UTF_8))) {
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                servers.add(inputLine);
            }
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        serversConnection.disconnect();
        // svData:
        // 0: JLabel - Label of server
        // 1: Bool - Server is selected (in UI)
        // 2: Bool - Server has password
        // 3: String - Server name
        // 4: String - Server password
        svData = new Object[servers.size() + 1][5];
        servers.add(0, "f" + localize("launch_without_server") + "|!");
        int j = 0;
        for (String server : servers) {
            String serverInfo = serverInfo(server);
            if (serverInfo.isEmpty()) {
                continue;
            }
            final int index = j;
            j++;
            svData[index][4] = serverInfo.substring(serverInfo.indexOf('!') + 1);
            serverInfo = serverInfo.substring(0, serverInfo.indexOf('!'));
            svData[index][2] = serverInfo.charAt(0) == 't';
            svData[index][3] = serverInfo.substring(serverInfo.indexOf('|') + 1);
            serverInfo = serverInfo.substring(1, serverInfo.indexOf('|'));
            if (serverInfo.length() > 45) {
                serverInfo = serverInfo.substring(0, 42) + "...";
            }
            serverInfo = "<html>".concat(serverInfo).concat("</html>");
            JLabel label = new JLabel(serverInfo, getImageIcon("toolbar/cb-"), SwingConstants.LEADING);
            label.setFont(new Font("Consolas", Font.PLAIN, 16));
            label.setMaximumSize(new Dimension(448, 25));
            label.setBorder(new javax.swing.border.EmptyBorder(3, 3, 3, 0));
            label.addMouseListener(new java.awt.event.MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent evt) {
                    if (!isLeftMouseButton(evt)) {
                        return;
                    }
                    JLabel source = (JLabel) evt.getSource();
                    if ((boolean) svData[index][1]) {
                        return;
                    }
                    svData[index][1] = true;
                    source.setIcon(getImageIcon("toolbar/cb+"));
                    for (int a = 0; a < svData.length; a++) {
                        if (a == index || svData[a][0] == null) {
                            continue;
                        }
                        svData[a][1] = false;
                        ((JLabel) svData[a][0]).setIcon(getImageIcon("toolbar/cb-"));
                    }
                }

                @Override
                public void mouseEntered(MouseEvent evt) {
                    JLabel source = (JLabel) evt.getSource();
                    source.setIcon(getImageIcon("toolbar/cb" + (((boolean) svData[index][1]) ? "++" : "--")));
                    source.setForeground(new Color(200, 200, 200));
                }

                @Override
                public void mouseExited(MouseEvent evt) {
                    JLabel source = (JLabel) evt.getSource();
                    source.setIcon(getImageIcon("toolbar/cb" + (((boolean) svData[index][1]) ? "+" : "-")));
                    source.setForeground(Color.WHITE);
                }
            });
            svData[index][0] = label;
            svData[index][1] = false;
            if (index == 0) {
                svData[0][1] = true;
                ((JLabel) svData[0][0]).setIcon(getImageIcon("toolbar/cb+"));
            }
            panelBot.add(label);
            jScrollPane2.setViewportView(panelBot);
        }
        lblServersStatus.setIcon(null);
        lblServersStatus.setToolTipText(null);
    }

    private static String serverInfo(String addr) {
        addr = addr.trim();
        if (addr.isEmpty() || addr.charAt(0) == '#') {
            return "";
        }
        if (addr.charAt(0) == 'f') {
            return addr;
        }

        String password = "";
        if (addr.indexOf('!') != -1) {
            password = addr.substring(addr.indexOf('!') + 1);
            addr = addr.substring(0, addr.indexOf('!'));
        }

        String addrName = addr.substring(0, addr.indexOf(':'));
        int addrPort = Integer.parseInt(addr.substring(addr.indexOf(':') + 1)) + 1;
        byte[] request1 = {127, 127, 127, 127, 84, 83, 111, 117, 114, 99, 101, 32, 69, 110, 103, 105, 110, 101, 32, 81, 117, 101, 114, 121, 0};
        byte[] buf = new byte[256];

        try (java.net.DatagramSocket clientSocket = new java.net.DatagramSocket(addrPort)) {
            clientSocket.setSoTimeout(500);
            clientSocket.connect(java.net.InetAddress.getByName(addrName), addrPort);
            java.net.DatagramPacket sendPacket = new java.net.DatagramPacket(request1, request1.length);
            clientSocket.send(sendPacket);
            java.net.DatagramPacket receivePacket = new java.net.DatagramPacket(buf, buf.length);
            clientSocket.receive(receivePacket);
            buf = receivePacket.getData();
            clientSocket.disconnect();
        } catch (IOException ex) {
            // System.out.println("Request timed out for " + addr);
            return "";
        }

        byte[] request2 = {
            127, 127, 127, 127, 84, 83, 111, 117, 114, 99, 101, 32, 69, 110, 103, 105, 110, 101, 32, 81, 117, 101, 114, 121, 0, buf[5], buf[6], buf[7], buf[8]
        };

        try (java.net.DatagramSocket clientSocket = new java.net.DatagramSocket(addrPort)) {
            clientSocket.setSoTimeout(500);
            clientSocket.connect(java.net.InetAddress.getByName(addrName), addrPort);
            java.net.DatagramPacket sendPacket = new java.net.DatagramPacket(request2, request2.length);
            clientSocket.send(sendPacket);
            java.net.DatagramPacket receivePacket = new java.net.DatagramPacket(buf, buf.length);
            clientSocket.receive(receivePacket);
            buf = receivePacket.getData();
            clientSocket.disconnect();
        } catch (IOException ex) {
            // System.out.println("Request timed out for " + addr);
            return "";
        }

        char hasPassword = 'f';
        boolean nameComplete = false;
        StringBuilder name = new StringBuilder();
        for (int i = 6; i < buf.length; i++) {
            byte b = buf[i];
            if (hasPassword == 'f' && b == 100 && buf[i + 2] == 1) {
                hasPassword = 't';
            }
            if (!nameComplete) {
                if (b == 0) {
                    nameComplete = true;
                } else {
                    name.append((char) b);
                }
            }
        }

        return (hasPassword + name.toString() + '|' + addr + '!' + password);
    }

    private void initComponents() {
        titleBar = new JLabel();
        btnExit = new com.mcotool.client.TLabel();
        btnLaunch = new JLabel();
        lblOptionals = new JLabel();
        jScrollPane1 = new JScrollPane();
        JList<String> lstMods = new JList<>();
        btnInstallOptionals = new JLabel();
        lblServers = new JLabel();
        jScrollPane2 = new JScrollPane();
        JList<String> lstServers = new JList<>();
        lblServersStatus = new JLabel();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        titleBar.setBackground(UI_TONE);
        titleBar.setIcon(new ImageIcon(getImageIcon("img/icon").getImage().getScaledInstance(22, 22, Image.SCALE_SMOOTH)));
        titleBar.setText(localize("launch_the_game"));
        titleBar.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        titleBar.setOpaque(true);
        titleBar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                titleBarMouseDragged(evt);
            }
        });
        titleBar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                titleBarMousePressed(evt);
            }
        });

        btnExit.setBackground(UI_TONE);
        btnExit.setHorizontalAlignment(SwingConstants.CENTER);
        btnExit.setIcon(new ImageIcon(getClass().getResource("/com/mcotool/client/rsc/toolbar/x.png")));
        btnExit.setToolTipText(localize("close"));
        btnExit.setFocusable(false);
        btnExit.setOpaque(true);
        btnExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnExitMouseClicked(evt);
            }

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnExitMouseEntered();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnExitMouseExited();
            }

            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnExitMousePressed(evt);
            }
        });

        btnLaunch.setBackground(UI_TONE);
        btnLaunch.setHorizontalAlignment(SwingConstants.CENTER);
        btnLaunch.setText(localize("launch"));
        btnLaunch.setOpaque(true);
        btnLaunch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnLaunchMouseClicked(evt);
            }

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnLaunchMouseEntered();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnLaunchMouseExited();
            }

            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnLaunchMousePressed(evt);
            }

            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnLaunchMouseReleased();
            }
        });

        lblOptionals.setText(localize("extra_mods"));

        lstMods.setBackground(Color.darkGray);
        jScrollPane1.setViewportView(lstMods);

        btnInstallOptionals.setBackground(UI_TONE);
        btnInstallOptionals.setHorizontalAlignment(SwingConstants.CENTER);
        btnInstallOptionals.setText(localize("get_extra_mods"));
        btnInstallOptionals.setOpaque(true);
        btnInstallOptionals.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnInstallOptionalsMouseClicked();
            }

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnInstallOptionalsMouseEntered();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnInstallOptionalsMouseExited();
            }

            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnInstallOptionalsMousePressed();
            }
        });

        lblServers.setText(localize("server_list"));

        lstServers.setBackground(Color.darkGray);
        jScrollPane2.setViewportView(lstServers);

        lblServersStatus.setIcon(new ImageIcon(getClass().getResource("/com/mcotool/client/rsc/img/search.gif")));
        lblServersStatus.setToolTipText(localize("searching_servers"));

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jScrollPane1, GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(titleBar, GroupLayout.PREFERRED_SIZE, 415, GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(btnExit, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(lblOptionals, GroupLayout.PREFERRED_SIZE, 400, GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(18, 18, 18)
                                                .addComponent(btnLaunch, GroupLayout.PREFERRED_SIZE, 412, GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(10, 10, 10)
                                                .addComponent(lblServers, GroupLayout.PREFERRED_SIZE, 400, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(lblServersStatus, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(jScrollPane2, GroupLayout.Alignment.TRAILING)
                        .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnInstallOptionals, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                                .addGap(12, 12, 12))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                        .addComponent(btnExit, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(titleBar, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                                .addGap(11, 11, 11)
                                .addComponent(lblOptionals)
                                .addGap(0, 8, Short.MAX_VALUE)
                                .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 196, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnInstallOptionals, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                                .addGap(12, 12, 12)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(lblServers)
                                        .addComponent(lblServersStatus, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jScrollPane2, GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                                .addGap(12, 12, 12)
                                .addComponent(btnLaunch, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                                .addGap(13, 13, 13))
        );

        pack();
    }

    private void titleBarMouseDragged(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            int X = getLocation().x;
            int Y = getLocation().y;
            setLocation(X + (X + evt.getX()) - (X + initialClick.x), Y + (Y + evt.getY()) - (Y + initialClick.y));
        }
    }

    private void titleBarMousePressed(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            initialClick = evt.getPoint();
        }
    }

    private void btnExitMouseClicked(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            isArmaRunningTimer.cancel();
            dispose();
        }
    }

    private void btnExitMouseEntered() {
        btnExit.setBackground(Color.RED);
    }

    private void btnExitMouseExited() {
        btnExit.setBackground(UI_TONE);
    }

    private void btnExitMousePressed(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            btnExit.setBackground(new Color(255, 64, 64));
        }
    }

    private void btnLaunchMouseClicked(java.awt.event.MouseEvent evt) {
        if (!isLeftMouseButton(evt)) {
            return;
        }
        btnLaunchMouseExited();
        StringBuilder launch = new StringBuilder("cmd.exe /c start \"Arma 3\" /High \"");
        launch.append(Database.get("path", null));
        launch.append("/arma3_x64.exe\" \"-mod=");
        launchMods.forEach(mod -> launch.append(mod).append(";"));
        for (Object[] mod : modData) {
            if ((boolean) mod[0]) {
                launch.append((String) mod[1]).append(";");
            }
        }
        launch.append("\"");
        launch.append(Database.get("launchParams", ""));
        String[] params = new String[]{"-noPause", "-noLand", "-noSplash", "-skipIntro", "-world=empty"};
        for (String param : params) {
            if (!toLower(launch.toString()).contains(toLower(param))) {
                launch.append(" ").append(param);
            }
        }
        for (int i = 1; i < svData.length; i++) {
            if (svData[i][0] == null || !(boolean) svData[i][1]) {
                continue;
            }
            String address = (String) svData[i][3];
            String port = address.substring(address.indexOf(':') + 1);
            address = address.substring(0, address.indexOf(':'));
            launch.append(" -connect=").append(address).append(" -port=").append(port);
            if ((boolean) svData[i][2]) {
                if (((String) svData[i][4]).isEmpty()) {
                    DlgPassword passDialog = new DlgPassword((Frame) getParent());
                    passDialog.setLocationRelativeTo(this);
                    passDialog.setVisible(true);
                    if (passDialog.isButton) {
                        launch.append(" -password=").append(passDialog.password);
                    } else {
                        return;
                    }
                } else {
                    launch.append(" -password=").append((String) svData[i][4]);
                }
            }
        }
        System.out.println(launch);
        try {
            Runtime.getRuntime().exec(launch.toString());
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        if (Database.getBool("exitOnLaunch", true)) {
            System.exit(2);
        }
    }

    private void btnLaunchMouseEntered() {
        if (btnLaunchEnabled) {
            btnLaunch.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 2));
        }
    }

    private void btnLaunchMouseExited() {
        if (btnLaunchEnabled) {
            btnLaunch.setBorder(null);
            btnLaunchMouseReleased();
        }
    }

    private void btnLaunchMousePressed(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt) && btnLaunchEnabled) {
            btnLaunch.setBackground(UI_FADE);
            btnLaunch.setBorder(null);
        }
    }

    private void btnInstallOptionalsMouseClicked() {
        /*if (!isLeftMouseButton(evt)) {
            return;
        }
        btnInstallOptionalsMouseExited(evt);*/
    }

    private void btnInstallOptionalsMouseEntered() {
        /*btnInstallOptionals.setBackground(UI_FADE);
        btnInstallOptionals.setForeground(Color.BLACK);*/
    }

    private void btnInstallOptionalsMouseExited() {
        /*btnInstallOptionals.setBackground(UI_TONE);
        btnInstallOptionals.setForeground(Color.WHITE);*/
    }

    private void btnInstallOptionalsMousePressed() {
        /*if (isLeftMouseButton(evt)) {
            btnInstallOptionals.setBackground(Color.WHITE);
        }*/
    }

    private void btnLaunchMouseReleased() {
        btnLaunch.setBackground(UI_TONE);
    }

    private com.mcotool.client.TLabel btnExit;
    private JLabel btnInstallOptionals;
    private JLabel btnLaunch;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JLabel lblOptionals;
    private JLabel lblServers;
    private JLabel lblServersStatus;
    private JLabel titleBar;
}
