/*
 * Copyright (C) 2015-2023 Ozan Egitmen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.mcotool.client.gui;

import com.mcotool.client.gui.dialogs.*;
import com.mcotool.client.Database;

import java.io.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

import static com.mcotool.client.Main.*;
import static javax.swing.SwingUtilities.isLeftMouseButton;

@SuppressWarnings({"resource", "ConstantConditions"})
public class FrmMain extends JFrame {

    private static final int FILE_DOWNLOAD_MAX_TRIES = 8;
    private boolean btnToggleDownloadEnabled, btnPlayEnabled, install;
    private final AtomicBoolean isDownloadPaused = new AtomicBoolean(false);
    private Point initialClick;
    private final ArrayList<String> mods = new ArrayList<>();
    private final JLabel pbInstallText = new JLabel();
    private final SystemTray tray = SystemTray.getSystemTray();
    private final TrayIcon trayIcon;

    public FrmMain(byte[] banner, String versionString) {
        getRootPane().setBorder(UI_BORDER);
        initComponents();
        for (Object[] oa : new Object[][]{
                {titleBar, 2, 16},
                {lblStatus, 0, 18},
                {lblProgress, 0, 18},
                {btnToggleDownload, 0, 18},
                {btnPlay, 0, 18},
                {pbInstall, 1, 13}
        }) {
            ((Component) oa[0]).setFont(font(oa[1], (int) oa[2]));
        }
        btnToggleDownload.setBackground(Color.GRAY);
        btnPlay.setBackground(Color.GRAY);
        lblBanner.setIcon(new ImageIcon(banner));
        pbInstall.setLayout(new GridBagLayout());
        pbInstallText.setForeground(Color.BLACK);
        pbInstallText.setFont(pbInstall.getFont().deriveFont(12.5F));
        pbInstallText.setVerticalAlignment(SwingConstants.TOP);
        pbInstallText.setHorizontalAlignment(SwingConstants.CENTER);
        pbInstallText.setMinimumSize(new Dimension(140, 20));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(4, 0, 5, 0);
        pbInstall.add(pbInstallText, gbc);
        setTitle("MCo Tool " + versionString.substring(0, 3));
        trayIcon = new TrayIcon(getIconImage(), "MCo Tool");
        trayIcon.setImageAutoSize(true);
        PopupMenu popup = new PopupMenu();
        MenuItem open = new MenuItem(localize("show"));
        open.addActionListener((ActionEvent e) -> leaveTray());
        popup.add(open);
        MenuItem close = new MenuItem(localize("close"));
        close.addActionListener((ActionEvent e) -> {
            if (install) {
                leaveTray();
            }
            btnExitMouseClicked(new java.awt.event.MouseEvent(rootPane, 0, 0L, 0, 0, 0, 0, false, 1));
        });
        popup.add(close);
        trayIcon.setPopupMenu(popup);
        trayIcon.addActionListener((ActionEvent e) -> leaveTray());
    }

    public void installer(String path, String pathTS, boolean forceDel, boolean ignoreTFAR) throws Exception {
        if (isProcessRunning("arma3")) {
            DlgOkay shutArma = showDlgOkay(this, 6);
            if (!shutArma.isButton) {
                System.exit(13);
            }
            shutArma.dispose();
        }

        install = true;
        System.out.println("Calculating differences...");
        HttpURLConnection databaseConnection = url("modfiles.dbzip");
        InputStream in = new java.util.zip.InflaterInputStream(databaseConnection.getInputStream());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[GLOBAL_BUFFER_SIZE];
        int len;
        while ((len = in.read(buffer)) > 0) {
            baos.write(buffer, 0, len);
        }
        baos.close();
        in.close();
        databaseConnection.disconnect();
        BufferedReader br = new BufferedReader(new StringReader(baos.toString()));
        String line;
        final ArrayList<String> head = new ArrayList<>(),
                optionalHead = new ArrayList<>();
        while ((line = br.readLine()) != null) {
            (line.contains(" -O ") ? optionalHead : head).add(line);
        }
        br.close();
        if (head.get(0).equals("bip")) {
            System.err.println("Server has bip");
            install = false;
            lblStatus.setText(localize("server_updating"));
            setValue(1);
            pbInstall.setForeground(new Color(255, 64, 64));
            return;
        }
        String lut = head.remove(head.size() - 1);
        ArrayList<String> hh = new ArrayList<>(head), optionalMods = new ArrayList<>();
        optionalHead.parallelStream().map(str -> str.substring(
                0, (str.contains("userconfig/")) ? str.lastIndexOf('/') : str.indexOf('/')
        )).filter(str -> !optionalMods.contains(toLower(str))).forEach(str -> optionalMods.add(toLower(str)));
        System.out.println("Optional mods: " + optionalMods);
        // TODO: Optional stuff
        head.parallelStream().map(str -> str.substring(
                0, (str.contains("userconfig/")) ? str.lastIndexOf('/') : str.indexOf('/')
        )).filter(str -> !mods.contains(toLower(str))).forEach(str -> mods.add(toLower(str)));
        if (Database.get("lastUpdateTime", "0").equals(lut.substring(lut.indexOf('@') + 1)) && !Database.getBool("tfarSkipped", false)) {
            finishInstall(lut);
            return;
        }
        System.out.println("Comparing for: " + mods);
        ArrayList<String> local = new ArrayList<>();

        for (String file : new File(path).list()) {
            if (!mods.contains(toLower(file))) {
                continue;
            }

            java.nio.file.Files.walk(java.nio.file.Paths.get(path + "/" + file)).parallel()
                    .filter(filePath -> !filePath.toFile().isDirectory() && !filePath.toString().endsWith(".chash"))
                    .forEach(filePath -> local.add(
                            toLower(filePath.toString())
                                    .replace('\\', '/')
                                    .replace(path + "/", "")
                                    + "|" + filePath.toFile().length()
                    ));
        }

        long lastUpdateTime = Long.parseLong(Database.get("lastUpdateTime", "0"));
        AtomicInteger calculatingCount = new AtomicInteger(0);
        ExecutorService calculatingJobs = Executors.newFixedThreadPool(4);

        for (int i = 0; i < head.size(); i++) {
            final int index = i;

            calculatingJobs.submit(() -> {
                int calculatingCountSafe = calculatingCount.getAndIncrement();

                lblStatus.setText(String.format(localize("calculating_differences_f"), calculatingCountSafe + 1, head.size()));
                String headStr = head.get(index);
                if (lastUpdateTime >= Long.parseLong(headStr.substring(headStr.indexOf('?') + 1, headStr.indexOf('*')))) {
                    head.set(index, "::");
                    return;
                }
                for (String loc : local) {
                    if (loc.contains(headStr.substring(0, headStr.indexOf('>')))
                            && hash(path + "/" + file(loc)).equals(headStr.substring(headStr.indexOf(':') + 1, headStr.indexOf('?')))) {
                        head.set(index, "::");
                        break;
                    }
                }
            });
        }

        calculatingJobs.shutdown();
        if (!calculatingJobs.isTerminated()) {
            //noinspection ResultOfMethodCallIgnored
            calculatingJobs.awaitTermination(10, java.util.concurrent.TimeUnit.MINUTES);
        }

        lblStatus.setText(localize("calculating_differences"));
        head.removeAll(java.util.Collections.singleton("::"));
        hh.replaceAll(this::file);
        for (int i = 0; i < local.size(); i++) {
            String k = file(local.get(i));
            for (String h : hh) {
                boolean s = !k.contains("/addons/");
                if (h.contains(k) || (forceDel ? k.contains(".pbo") && s : !k.contains(".pbo") || s)) {
                    local.set(i, "::");
                    break;
                }
            }
        }
        local.removeAll(java.util.Collections.singleton("::"));
        if (head.size() + local.size() == 0) {
            if (Database.getBool("tfarSkipped", false) || Database.get("lastUpdateTime", "DEF").equals("0")) {
                updateTFAR(pathTS, ignoreTFAR, lut);
            }
            finishInstall(lut);
            return;
        } else if (!Database.getBool("hideFileList", false)) {
            leaveTray();
            if (getState() == 1) {
                setState(0);
            }
            DlgFiles filesList = new DlgFiles(this, head, local);
            filesList.setLocationRelativeTo(this);
            filesList.setVisible(true);
            if (!filesList.isButton) {
                System.exit(8);
            }
            filesList.dispose();
        }

        // TODO: Show optional mods dialog
        // optionalMods, optionalHead
        System.out.printf("Will update %d files and remove %d files\n", head.size(), local.size());
        lblStatus.setText(localize("starting_download"));
        Timer timerStart = new Timer();
        timerStart.schedule(lblStatusDots(), 0, 250);
        long totalSize = 0;
        if (head.size() > 0) {
            for (String str : head) {
                totalSize += Integer.parseInt(str.substring(str.indexOf('>') + 1, str.indexOf(':')));
            }
            progress(0, totalSize);
        }
        System.out.println("Total download size: " + byteToSize(totalSize));
        timerStart.cancel();
        if (new File(path).getFreeSpace() < totalSize * 1.6) {
            System.err.println("Arma drive doesn't have enough space!");
            showDlgOkay(this, 5);
            System.exit(11);
        }

        ExecutorService downloadJobs = Executors.newFixedThreadPool(3);
        ExecutorService extractJobs = Executors.newFixedThreadPool(4);
        btnToggleDownloadEnabled = true;
        btnToggleDownload.setBackground(UI_TONE);

        AtomicInteger downloadingCount = new AtomicInteger(0);
        AtomicLong totalDownloaded = new AtomicLong(0);

        for (int i = 0; i < head.size(); i++) {
            final int index = i;

            downloadJobs.submit(() -> {
                int downloadingCountSafe = downloadingCount.getAndIncrement();
                int downloadAttemptCount = 0;
                String str = head.get(index);
                lblStatus.setText(String.format(localize("file_downloading_f"), downloadingCountSafe + 1, head.size()));

                while (true) {
                    File temp = new File(WORK_DIR + "temp/" + file(str).substring(file(str).lastIndexOf("/") + 1) + ".7z");
                    delete(temp);

                    String downloadThreadName = Thread.currentThread().getName();
                    System.out.printf("[%s]: Started downloading '%s'\n", downloadThreadName, temp);

                    int downloaded = 0;

                    try {
                        HttpURLConnection connection = (HttpURLConnection) new URL(str.substring(str.indexOf('*') + 1)).openConnection();

                        int b;
                        byte[] data = new byte[GLOBAL_BUFFER_SIZE];
                        BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
                        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(temp));

                        while ((b = bis.read(data)) >= 0) {
                            downloaded += b;
                            totalDownloaded.addAndGet(b);
                            bos.write(data, 0, b);

                            if (Thread.currentThread().isInterrupted()) {
                                break;
                            }

                            synchronized (isDownloadPaused) {
                                while (isDownloadPaused.get()) {
                                    isDownloadPaused.wait();
                                }
                            }
                        }

                        bis.close();
                        bos.close();
                        connection.disconnect();

                        if (extractJobs.isShutdown()) {
                            delete(temp);
                            return;
                        }

                        System.out.printf("[%s]: Finished downloading '%s'\n", downloadThreadName, temp);

                        extractJobs.submit(() -> {
                            String fileName = path + "/" + file(str);
                            String extractThreadName = Thread.currentThread().getName();
                            System.out.printf("[%s]: Started extracting '%s'\n", extractThreadName, fileName);
                            delModFile(fileName);

                            // Create the parent dir of fileName so 7-zip doesn't have to bother creating it
                            // This prevents a race condition where multiple instances of 7-zip try to create the parent directory simultaneously
                            // The one(s) who lose the race complain that the parent dir already exists and they fail
                            //noinspection ResultOfMethodCallIgnored
                            new File(fileName).getParentFile().mkdirs();

                            try {
                                Process ext = new ProcessBuilder(WORK_DIR + "ext_asst.exe", "\"extractfiles" + fileName + "\"").start();
                                ext.waitFor();

                                if (Thread.currentThread().isInterrupted()) {
                                    ext.destroy();
                                    delModFile(fileName);
                                } else if (!String.valueOf(new File(fileName).length()).equals(str.substring(str.indexOf('|') + 1, str.indexOf('>')))) {
                                    System.err.printf("[%s]: Received file '%s' is too short! Aborting...\n", extractThreadName, fileName);
                                    delModFile(fileName);

                                    downloadJobs.shutdownNow();
                                    extractJobs.shutdownNow();
                                } else {
                                    System.out.printf("[%s]: Finished extracting '%s'\n", extractThreadName, fileName);
                                }
                            } catch (IOException ex) {
                                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                            } catch (InterruptedException ex) {
                                delModFile(fileName);
                            }
                        });

                        return;
                    } catch (IOException ex) {
                        if (downloadAttemptCount == FILE_DOWNLOAD_MAX_TRIES - 1) {
                            System.err.printf("[%s]: Lost connection to the server! Aborting...\n", downloadThreadName);
                            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                            downloadJobs.shutdownNow();
                            extractJobs.shutdownNow();

                            return;
                        } else {
                            System.err.printf("[%s]: Lost connection to the server! Retrying...\n", downloadThreadName);
                            totalDownloaded.addAndGet(downloaded * -1);
                            downloadAttemptCount++;

                            try {
                                //noinspection BusyWait
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                delete(temp);
                                return;
                            }
                        }
                    } catch (InterruptedException ex) {
                        delete(temp);
                        return;
                    }
                }
            });
        }

        downloadJobs.shutdown();

        while (!downloadJobs.isTerminated()) {
            long totalDownloadedSafe = totalDownloaded.get();
            double downloadedRatio = (double) totalDownloadedSafe / (double) totalSize;
            setValue(downloadedRatio);
            pbInstallText.setText(String.format("%.1f%%", downloadedRatio * 100));
            progress(totalDownloadedSafe, totalSize);
            trayIcon.setToolTip("MCo Tool\n" + lblProgress.getText());

            //noinspection BusyWait
            Thread.sleep(GLOBAL_SCHEDULER_TIME);

            if (extractJobs.isShutdown()) {
                break;
            }
        }

        if (extractJobs.isShutdown()) {
            new Thread(() -> {
                if (tray.getTrayIcons().length > 0) {
                    trayIcon.displayMessage(localize("lost_connection_title"), localize("lost_connection"), TrayIcon.MessageType.ERROR);
                    leaveTray();
                }
                showDlgOkay(this, 1);
                System.exit(9);
            }).start();

            return;
        }

        btnToggleDownloadEnabled = false;
        btnToggleDownload.setBackground(Color.GRAY);
        setIndeterminate(true);
        lblStatus.setText(localize("finishing_install"));
        Timer timerFinish = new Timer();
        timerFinish.schedule(lblStatusDots(), 0, 250);

        local.forEach(f -> delModFile(path + "/" + file(f)));
        delEmptyDirs(new File(path));
        for (String h : head) {
            if (h.contains("task_force") || h.contains("tfar") || Database.get("lastUpdateTime", "0").equals("0")) {
                updateTFAR(pathTS, ignoreTFAR, lut);
                break;
            }
        }

        extractJobs.shutdown();
        if (!extractJobs.isTerminated()) {
            //noinspection ResultOfMethodCallIgnored
            extractJobs.awaitTermination(30, java.util.concurrent.TimeUnit.MINUTES);
        }

        timerFinish.cancel();
        System.out.println("Installation is complete!");
        lblStatus.setText(localize("install_complete"));
        trayIcon.setToolTip("MCo Tool\n" + localize("install_complete"));
        trayIcon.displayMessage("MCo Tool", localize("install_complete"), TrayIcon.MessageType.INFO);
        setIndeterminate(false);
        progress(totalSize, totalSize);
        finishInstall(lut);
    }

    public void leaveTray() {
        setVisible(true);
        tray.remove(trayIcon);
    }

    private String file(String o) {
        return o.substring(0, o.indexOf('|'));
    }

    private void setValue(double val) {
        pbInstall.setValue((int) (val * (double) pbInstall.getMaximum()));
    }

    private void progress(long a, long b) {
        lblProgress.setText(localize("total") + ": " + byteToSize(a) + " / " + byteToSize(b));
    }

    private String byteToSize(long bytes) {
        if (bytes <= 0) {
            return "0 B";
        }
        int exp = (int) (Math.log(bytes) / Math.log(1024));
        return String.format("%.1f %sB", bytes / Math.pow(1024, exp), new String[]{"", "K", "M", "G"}[exp]);
    }

    private void finishInstall(String lut) {
        setValue(1);
        pbInstall.setForeground(new Color(64, 255, 64));
        lblStatus.setText(localize("mods_are_uptodate"));
        Database.set("lastUpdateTime", lut.substring(lut.indexOf('@') + 1));
        btnPlayEnabled = true;
        btnPlay.setBackground(UI_TONE);
        install = false;
    }

    private String hash(String file) {
        String hash = null;

        try {

            Process hasher = new ProcessBuilder(WORK_DIR + "ext_asst.exe", file).start();
            try (
                    InputStream is = hasher.getInputStream();
                    InputStreamReader isr = new InputStreamReader(is);
                    BufferedReader br = new BufferedReader(isr)
            ) {
                hash = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }

        return hash;
    }

    @SuppressWarnings("ConstantConditions")
    private void delEmptyDirs(File file) {
        if (!file.isDirectory()) {
            return;
        }

        for (File subFile : file.listFiles()) {
            delEmptyDirs(subFile);
        }
        if (file.list().length == 0) {
            delete(file);
        }
    }

    // TODO: chash files aren't getting deleted?
    private void delModFile(String fileName) {
        delete(fileName);
        if (fileName.endsWith(".pbo")) {
            delete(fileName + ".chash");
        }
    }

    private java.util.TimerTask lblStatusDots() {
        return new java.util.TimerTask() {
            @Override
            public void run() {
                String text = lblStatus.getText();
                lblStatus.setText(
                        (text.length() - text.replace(".", "").length() < 3)
                                ? (text + ".")
                                : (text.substring(0, text.length() - 3))
                );
            }
        };
    }

    private void updateTFAR(String pathTS, boolean ignoreTFAR, String lut) throws Exception {
        if (ignoreTFAR) {
            System.out.println("Ignoring TFAR update");
            return;
        }

        System.out.println("Updating TS plugins for TFAR...");
        String url = lut.substring(lut.indexOf('(') + 1, lut.indexOf(')'));
        HttpURLConnection pluginConnection = (HttpURLConnection) new URL(url).openConnection();
        BufferedInputStream bis = new BufferedInputStream(pluginConnection.getInputStream());
        String fileName = WORK_DIR + "temp/task_force_radio.ts3_plugin";
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(fileName));
        byte[] data = new byte[GLOBAL_BUFFER_SIZE];
        int i;
        while ((i = bis.read(data)) >= 0) {
            bos.write(data, 0, i);
        }
        bos.close();
        bis.close();
        pluginConnection.disconnect();

        DlgOkay shutTS = showDlgOkay(this, 0);
        if (!shutTS.isButton) {
            Database.setBool("tfarSkipped", true);
            System.exit(3);
        }
        shutTS.dispose();

        Process packageInst = new ProcessBuilder(pathTS + "/package_inst.exe", fileName).start();
        packageInst.waitFor();
        delete(fileName);

        System.out.println("TS plugins updated!");
        Database.set("tfarSkipped", null);
    }

    private Timer timerIndeterminate;

    private void setIndeterminate(boolean indeterminate) {
        if (indeterminate) {
            pbInstallText.setText("");
            setValue(0);
            timerIndeterminate = new Timer();
            timerIndeterminate.schedule(new java.util.TimerTask() {
                private int w = 0, x = 0, s = 1;

                @Override
                public void run() {
                    Graphics2D g = (Graphics2D) pbInstall.getGraphics();
                    g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
                    g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);

                    g.setColor(UI_TONE);
                    if (w < 40 && s == 1) {
                        g.fillRect(x + 2, 2, w, 20);
                        w += 2;
                    } else {
                        if (x + 40 > 594) {
                            w -= 2;
                            s = 0;
                        }
                        g.fillRect(x + 4, 2, w, 20);
                        g.setColor(new Color(230, 230, 230));
                        g.fillRect(x + 2, 2, 2, 20);
                        if (x > 594) {
                            g.setColor(new Color(64, 64, 64));
                            g.fillRect(x + 2, 2, 2, 20);
                            w = 0;
                            x = 0;
                            s = 1;
                        } else {
                            x += 2;
                        }
                    }
                }
            }, 0, 20); // Gets us ~50fps
        } else {
            repaint();
            if (timerIndeterminate != null) {
                timerIndeterminate.cancel();
            }
        }
    }

    private void initComponents() {
        titleBar = new JLabel();
        btnSettings = new com.mcotool.client.TLabel();
        btnMinimize = new com.mcotool.client.TLabel();
        btnExit = new com.mcotool.client.TLabel();
        lblStatus = new JLabel();
        pbInstall = new JProgressBar();
        lblProgress = new JLabel();
        btnToggleDownload = new JLabel();
        JSeparator jSeparator = new JSeparator();
        lblBanner = new JLabel();
        btnPlay = new JLabel();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getImageIcon("img/icon").getImage());
        setUndecorated(true);
        setResizable(false);

        titleBar.setBackground(UI_TONE);
        titleBar.setIcon(new ImageIcon(getImageIcon("img/icon").getImage().getScaledInstance(22, 22, Image.SCALE_SMOOTH)));
        titleBar.setText(localize("main_title"));
        titleBar.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        titleBar.setOpaque(true);
        titleBar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                titleBarMouseDragged(evt);
            }
        });
        titleBar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                titleBarMousePressed(evt);
            }
        });

        btnSettings.setBackground(UI_TONE);
        btnSettings.setHorizontalAlignment(SwingConstants.CENTER);
        btnSettings.setIcon(new ImageIcon(getClass().getResource("/com/mcotool/client/rsc/img/gear.png")));
        btnSettings.setToolTipText(localize("settings"));
        btnSettings.setOpaque(true);
        btnSettings.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSettingsMouseClicked(evt);
            }

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSettingsMouseEntered();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSettingsMouseExited();
            }

            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnSettingsMousePressed(evt);
            }

            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnSettingsMouseReleased();
            }
        });

        btnMinimize.setBackground(UI_TONE);
        btnMinimize.setHorizontalAlignment(SwingConstants.CENTER);
        btnMinimize.setIcon(new ImageIcon(getClass().getResource("/com/mcotool/client/rsc/toolbar/-.png")));
        btnMinimize.setToolTipText(localize("minimize"));
        btnMinimize.setOpaque(true);
        btnMinimize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseClicked(evt);
            }

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseEntered();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseExited();
            }

            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnMinimizeMousePressed(evt);
            }

            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseReleased();
            }
        });

        btnExit.setBackground(UI_TONE);
        btnExit.setHorizontalAlignment(SwingConstants.CENTER);
        btnExit.setIcon(new ImageIcon(getClass().getResource("/com/mcotool/client/rsc/toolbar/x.png")));
        btnExit.setToolTipText(localize("close"));
        btnExit.setOpaque(true);
        btnExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnExitMouseClicked(evt);
            }

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnExitMouseEntered();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnExitMouseExited();
            }

            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnExitMousePressed(evt);
            }
        });

        lblStatus.setText(localize("calculating_differences"));

        pbInstall.setForeground(UI_TONE);
        pbInstall.setMaximum(596);
        pbInstall.setBorder(BorderFactory.createLineBorder(new Color(64, 64, 64), 2));
        pbInstall.setString("");
        pbInstall.setStringPainted(true);

        lblProgress.setHorizontalAlignment(SwingConstants.TRAILING);

        btnToggleDownload.setHorizontalAlignment(SwingConstants.CENTER);
        btnToggleDownload.setText(localize("pause_download"));
        btnToggleDownload.setOpaque(true);
        btnToggleDownload.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnToggleDownloadMouseClicked(evt);
            }

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnToggleDownloadMouseEntered();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnToggleDownloadMouseExited();
            }

            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnToggleDownloadMousePressed(evt);
            }

            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnToggleDownloadMouseReleased();
            }
        });

        jSeparator.setBackground(new Color(200, 200, 200));

        lblBanner.setIcon(new ImageIcon(getClass().getResource("/com/mcotool/client/rsc/img/banner.png")));

        btnPlay.setBackground(UI_TONE);
        btnPlay.setHorizontalAlignment(SwingConstants.CENTER);
        btnPlay.setText(localize("play"));
        btnPlay.setOpaque(true);
        btnPlay.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnPlayMouseClicked(evt);
            }

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPlayMouseEntered();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPlayMouseExited();
            }

            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnPlayMousePressed(evt);
            }

            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnPlayMouseReleased();
            }
        });

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(titleBar, GroupLayout.PREFERRED_SIZE, 545, GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnSettings, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(btnMinimize, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(btnExit, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                        .addComponent(btnToggleDownload, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                                                        .addGap(321, 321, 321)
                                                        .addComponent(btnPlay, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                                                        .addGap(2, 2, 2))
                                                .addGroup(layout.createSequentialGroup()
                                                        .addGap(25, 25, 25)
                                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                                .addGroup(layout.createSequentialGroup()
                                                                        .addGap(1, 1, 1)
                                                                        .addComponent(lblStatus, GroupLayout.PREFERRED_SIZE, 328, GroupLayout.PREFERRED_SIZE)
                                                                        .addGap(0, 0, 0)
                                                                        .addComponent(lblProgress, GroupLayout.PREFERRED_SIZE, 269, GroupLayout.PREFERRED_SIZE))
                                                                .addComponent(pbInstall, GroupLayout.PREFERRED_SIZE, 600, GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(25, 25, 25)
                                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(jSeparator, GroupLayout.PREFERRED_SIZE, 600, GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(lblBanner, GroupLayout.PREFERRED_SIZE, 600, GroupLayout.PREFERRED_SIZE))))
                                .addGap(25, 25, 25))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(titleBar, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnSettings, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnMinimize, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnExit, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                                .addGap(11, 11, 11)
                                .addComponent(lblBanner)
                                .addGap(9, 9, 9)
                                .addComponent(jSeparator, GroupLayout.PREFERRED_SIZE, 9, GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblStatus)
                                        .addComponent(lblProgress))
                                .addGap(7, 7, 7)
                                .addComponent(pbInstall, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                                .addGap(9, 9, 9)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnToggleDownload, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnPlay, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                                .addGap(19, 19, 19))
        );

        pack();
        setLocationRelativeTo(null);
    }

    private void titleBarMouseDragged(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            int X = getLocation().x;
            int Y = getLocation().y;
            setLocation(X + (X + evt.getX()) - (X + initialClick.x), Y + (Y + evt.getY()) - (Y + initialClick.y));
        }
    }

    private void titleBarMousePressed(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            initialClick = evt.getPoint();
        }
    }

    private void btnSettingsMouseClicked(java.awt.event.MouseEvent evt) {
        if (!isLeftMouseButton(evt)) {
            return;
        }
        btnSettingsMouseExited();
        DlgSettings settings = new DlgSettings(this);
        settings.setLocationRelativeTo(this);
        settings.setVisible(true);

        if (!settings.newLanguage.isEmpty() && settings.isButton) {
            titleBar.setText(localize("main_title"));
            btnToggleDownload.setText(isDownloadPaused.get() ? localize("continue_download") : localize("pause_download"));
            btnPlay.setText(localize("play"));
            btnSettings.setToolTipText(localize("settings"));
            btnMinimize.setToolTipText(localize("minimize"));
            btnExit.setToolTipText(localize("cancel"));
            if (!install) {
                lblStatus.setText(localize("mods_are_uptodate"));
            }
            // Also try to change lblProgress and lblStatus during install?
        }

        settings.dispose();
    }

    private void btnSettingsMouseEntered() {
        btnSettings.setBackground(UI_FADE);
    }

    private void btnSettingsMouseExited() {
        btnSettings.setBackground(UI_TONE);
        btnSettingsMouseReleased();
    }

    private void btnSettingsMousePressed(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            btnSettings.setIcon(getImageIcon("img/gearOver"));
        }
    }

    private void btnMinimizeMouseClicked(java.awt.event.MouseEvent evt) {
        if (!isLeftMouseButton(evt)) {
            return;
        }
        if (Database.getBool("minimizeToTray", false)) {
            try {
                tray.add(trayIcon);
            } catch (AWTException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }
            setVisible(false);
        } else {
            setState(1);
        }
    }

    private void btnMinimizeMouseEntered() {
        btnMinimize.setBackground(UI_FADE);
    }

    private void btnMinimizeMouseExited() {
        btnMinimize.setBackground(UI_TONE);
        btnMinimizeMouseReleased();
    }

    private void btnMinimizeMousePressed(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            btnMinimize.setIcon(getImageIcon("toolbar/--"));
        }
    }

    private void btnExitMouseClicked(java.awt.event.MouseEvent evt) {
        if (!isLeftMouseButton(evt)) {
            return;
        }
        if (install) {
            btnExitMouseExited();
            DlgOkay installInProgress = showDlgOkay(this, 2);
            if (installInProgress.isButton) {
                System.exit(7);
            }
            installInProgress.dispose();
        } else {
            System.exit(0);
        }
    }

    private void btnExitMouseEntered() {
        btnExit.setBackground(Color.RED);
    }

    private void btnExitMouseExited() {
        btnExit.setBackground(UI_TONE);
    }

    private void btnExitMousePressed(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt)) {
            btnExit.setBackground(new Color(255, 64, 64));
        }
    }

    private void btnToggleDownloadMouseClicked(java.awt.event.MouseEvent evt) {
        if (!isLeftMouseButton(evt) || !btnToggleDownloadEnabled) {
            return;
        }
        btnToggleDownloadMouseEntered();

        synchronized (isDownloadPaused) {
            isDownloadPaused.getAndSet(!isDownloadPaused.get()); // Toggle
            isDownloadPaused.notifyAll();
        }

        boolean paused = isDownloadPaused.get();

        if (paused) {
            trayIcon.setToolTip(trayIcon.getToolTip().replace("MCo Tool", "MCo Tool - " + localize("paused")));
        }

        btnToggleDownload.setText(paused ? localize("continue_download") : localize("pause_download"));
        String text = lblStatus.getText();
        lblStatus.setText(paused ? text + " (" + localize("paused") + ")" : text.substring(0, text.lastIndexOf(' ')));
        pbInstall.setForeground(paused ? new Color(255, 128, 0) : UI_TONE);
    }

    private void btnToggleDownloadMouseEntered() {
        if (btnToggleDownloadEnabled) {
            btnToggleDownload.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 2));
        }
    }

    private void btnToggleDownloadMouseExited() {
        if (btnToggleDownloadEnabled) {
            btnToggleDownload.setBorder(null);
            btnToggleDownloadMouseReleased();
        }
    }

    private void btnToggleDownloadMousePressed(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt) && btnToggleDownloadEnabled) {
            btnToggleDownload.setBackground(UI_FADE);
            btnToggleDownload.setBorder(null);
        }
    }

    private void btnPlayMouseClicked(java.awt.event.MouseEvent evt) {
        if (!isLeftMouseButton(evt) || !btnPlayEnabled) {
            return;
        }
        btnPlayMouseExited();
        DlgLaunch launch = new DlgLaunch(this, mods);
        launch.setLocationRelativeTo(this);
        launch.setVisible(true);
    }

    private void btnPlayMouseEntered() {
        if (btnPlayEnabled) {
            btnPlay.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 2));
        }
    }

    private void btnPlayMouseExited() {
        if (btnPlayEnabled) {
            btnPlay.setBorder(null);
            btnPlayMouseReleased();
        }
    }

    private void btnPlayMousePressed(java.awt.event.MouseEvent evt) {
        if (isLeftMouseButton(evt) && btnPlayEnabled) {
            btnPlay.setBackground(UI_FADE);
            btnPlay.setBorder(null);
        }
    }

    private void btnPlayMouseReleased() {
        if (btnPlayEnabled) {
            btnPlay.setBackground(UI_TONE);
        }
    }

    private void btnToggleDownloadMouseReleased() {
        if (btnToggleDownloadEnabled) {
            btnToggleDownload.setBackground(UI_TONE);
        }
    }

    private void btnSettingsMouseReleased() {
        btnSettings.setIcon(getImageIcon("img/gear"));
    }

    private void btnMinimizeMouseReleased() {
        btnMinimize.setIcon(getImageIcon("toolbar/-"));
    }

    private com.mcotool.client.TLabel btnExit;
    private com.mcotool.client.TLabel btnMinimize;
    private JLabel btnPlay;
    private com.mcotool.client.TLabel btnSettings;
    private JLabel btnToggleDownload;
    private JLabel lblBanner;
    private JLabel lblProgress;
    private JLabel lblStatus;
    private JProgressBar pbInstall;
    private JLabel titleBar;
}
