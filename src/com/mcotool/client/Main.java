/*
 * Copyright (C) 2015-2023 Ozan Egitmen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.mcotool.client;

import com.mcotool.client.gui.FrmMain;
import com.mcotool.client.gui.dialogs.DlgOkay;
import com.mcotool.client.gui.FrmSplash;

import java.awt.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.border.*;

@SuppressWarnings("ConstantConditions")
public class Main {

    public static final String WORK_DIR = getWorkDir();
    public static final Color UI_TONE = new Color(0, 127, 223), UI_FADE = new Color(32, 159, 255);
    public static final Border UI_BORDER = new LineBorder(UI_TONE);
    public static final int GLOBAL_BUFFER_SIZE = 8192, GLOBAL_SCHEDULER_TIME = 50;
    public static Font[] FONTS = new Font[3]; // Light, Regular, SemiBold
    private static final Properties STR_TABLE = new Properties();

    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        for (String key : new String[]{"Panel", "ToolBar", "ToolTip", "Button", "ComboBox"}) {
            UIManager.put(key + ".background", Color.DARK_GRAY);
        }
        for (String key : new String[]{"TextField", "Label", "ToolTip", "ComboBox", "ToggleButton", "List", "PasswordField"}) {
            UIManager.put(key + ".foreground", Color.WHITE);
        }
        for (Object[] key : new Object[][]{
                {"ToolTip.border", new CompoundBorder(new LineBorder(Color.BLACK), new EmptyBorder(0, 2, 2, 3))},
                {"ToolTip.font", new Font("Segoe UI", Font.PLAIN, 13)},
                {"ScrollBar.track", Color.LIGHT_GRAY},
                {"ScrollBar.trackHighlight", Color.LIGHT_GRAY},
                {"ScrollBar.thumb", UI_TONE},
                {"ScrollBar.thumbDarkShadow", UI_TONE},
                {"ScrollBar.thumbHighlight", UI_TONE},
                {"ScrollBar.thumbShadow", UI_TONE},
                {"ScrollBar.minimumThumbSize", new Dimension(0, 30)},
                {"TextField.background", new Color(15, 15, 15)},
                {"TextField.caretForeground", Color.WHITE},
                {"PasswordField.background", new Color(15, 15, 15)},
                {"PasswordField.caretForeground", Color.WHITE},
                {"ComboBox.font", new Font("Segoe UI", Font.PLAIN, 14)},
                {"ComboBox.selectionBackground", UI_TONE},
                {"ComboBox.selectionBackground", UI_TONE},
                {"List.selectionBackground", UI_TONE},
                {"List.selectionForeground", Color.WHITE},
                {"Label.font", new Font("Segoe UI", Font.PLAIN, 14)},
                {"ToolBar.shadow", Color.DARK_GRAY}
        }) {
            UIManager.put(key[0], key[1]);
        }
        ToolTipManager.sharedInstance().setInitialDelay(0);
        ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);
        int versionMain = 2560;
        String versionString = "2.5.6.0 (R61)";
        FrmSplash splash = new FrmSplash(versionString);
        splash.setVisible(true);
        byte[] banner = splash.start(versionMain, args);
        FrmMain start = new FrmMain(banner, versionString);
        start.setVisible(true);
        start.setAlwaysOnTop(true);
        start.setAlwaysOnTop(false);
        start.installer(splash.path, splash.pathTS, splash.info[2] > 0, splash.info[3] > 0);
    }

    public static String localize(String key) {
        return STR_TABLE.getProperty(key);
    }

    public static String toLower(String string) {
        return string.toLowerCase(java.util.Locale.ENGLISH);
    }

    public static Font font(Object type, int size) {
        return FONTS[(int) type].deriveFont((float) size);
    }

    public static ImageIcon getImageIcon(String file) {
        return new ImageIcon(Main.class.getResource("/com/mcotool/client/rsc/" + file + ".png"));
    }

    public static HttpURLConnection url(String file) {
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) new java.net.URL("http://mts.mcore.xyz/" + file.replace(" ", "%20")).openConnection();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }

    public static DlgOkay showDlgOkay(Frame body, int mode) {
        if (body instanceof com.mcotool.client.gui.FrmMain) {
            ((com.mcotool.client.gui.FrmMain) body).leaveTray();
            if (body.getState() == 1) {
                body.setState(0);
            }
        }
        DlgOkay dlgOkay = new DlgOkay(body, mode);
        dlgOkay.setLocationRelativeTo(body);
        dlgOkay.setVisible(true);
        return dlgOkay;
    }

    public static boolean isProcessRunning(String process) {
        StringBuilder sb = new StringBuilder();
        ProcessBuilder tasklist = new ProcessBuilder("C:/Windows/SysWOW64/tasklist.exe");
        try (BufferedReader br = new BufferedReader(new InputStreamReader(tasklist.start().getInputStream()))) {
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(toLower(line));
            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sb.toString().contains(process);
    }

    public static void delete(String file) {
        delete(new File(file));
    }

    @SuppressWarnings("ConstantConditions")
    public static void delete(File file) {
        String threadName = Thread.currentThread().getName();
        if (file.isDirectory()) {
            if (file.exists()) {
                if (file.list().length != 0) {
                    for (File temp : file.listFiles()) {
                        delete(temp);
                    }
                }
                //noinspection ResultOfMethodCallIgnored
                file.delete();
                System.out.printf("[%s]: Directory '%s' is deleted\n", threadName, file.getPath());
            }
        } else if (file.delete()) {
            System.out.printf("[%s]: File '%s' is deleted\n", threadName, file.getPath());
        } else if (file.exists()) {
            System.err.printf("[%s]: ERROR: File '%s' couldn't be deleted\n", threadName, file.getPath());
        }
    }

    public static String fileChooser(String title, String path, Component parent, boolean closeOnCancel) {
        String selectedPath = "";
        JFileChooser chooser = new JFileChooser();
        prepFC(chooser);
        chooser.setFileFilter(new javax.swing.filechooser.FileNameExtensionFilter(".exe", "exe"));
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setControlButtonsAreShown(false);
        chooser.setFileHidingEnabled(false);
        chooser.setDialogTitle(title);
        chooser.setCurrentDirectory(new File(path));
        if (chooser.showOpenDialog(parent) == 0) {
            selectedPath = chooser.getCurrentDirectory().toString();
        } else {
            chooser.cancelSelection();
            if (closeOnCancel) {
                System.exit(6);
            }
        }
        secondCombo = false;
        label = button = toggleButton = 0;
        return selectedPath;
    }

    private static boolean secondCombo = false;
    private static int label = 0, button = 0, toggleButton = 0;

    private static void prepFC(Container c) {
        for (Component cmp : c.getComponents()) {
            if (cmp instanceof JToolBar) {
                ((JToolBar) cmp).putClientProperty("XPStyle.subAppName", null);
            }
            if (cmp instanceof JToggleButton) {
                if (toggleButton == 4) {
                    cmp.setEnabled(false);
                }
                ((JToggleButton) cmp).setText(localize(new String[]{"recent_items", "desktop", "documents", "this_pc", "network"}[toggleButton++]));
            }
            if (cmp instanceof JList) {
                cmp.setBackground(Color.DARK_GRAY);
            }
            if (cmp instanceof JButton) {
                if (button == 3) {
                    cmp.setVisible(false);
                }
                button++;
            }
            if (cmp instanceof JComboBox) {
                if (secondCombo) {
                    cmp.setVisible(false);
                }
                secondCombo = true;
                cmp.setForeground(Color.BLACK);
            }
            if (cmp instanceof JLabel lbl) {
                switch (label) {
                    case 0 -> lbl.setText(localize("look_in"));
                    case 1 -> {
                        lbl.setBorder(new CompoundBorder(new LineBorder(Color.DARK_GRAY), new EmptyBorder(6, 0, 0, 0)));
                        lbl.setText(localize("file_name"));
                    }
                    case 2 -> lbl.setVisible(false);
                }
                label++;
            }
            if (cmp instanceof Container) {
                prepFC((Container) cmp);
            }
        }
    }

    public static void setLanguage(String lang) {
        InputStream stream = Main.class.getResourceAsStream("/com/mcotool/client/lang/" + lang + ".properties");
        try {
            STR_TABLE.load(stream);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static String getWorkDir() {
        String workDir = System.getenv("MCO_TOOL_WORK_DIR");
        if (workDir == null) {
            workDir = urlToFile(getLocation(Main.class).toString()).getParent();
        }

        return toLower(workDir.replace('\\', '/')) + "/";
    }

    // Everything below is from ctrueden's answer here:
    // https://stackoverflow.com/questions/320542/how-to-get-the-path-of-a-running-jar-file

    /**
     * Gets the base location of the given class.
     * <p>
     * If the class is directly on the file system (e.g.,
     * "/path/to/my/package/MyClass.class") then it will return the base directory
     * (e.g., "file:/path/to").
     * </p>
     * <p>
     * If the class is within a JAR file (e.g.,
     * "/path/to/my-jar.jar!/my/package/MyClass.class") then it will return the
     * path to the JAR (e.g., "file:/path/to/my-jar.jar").
     * </p>
     *
     * @param c The class whose location is desired.
     */
    public static URL getLocation(final Class<?> c) {
        if (c == null) {
            return null; // could not load the class
        }

        // try the easy way first
        try {
            final URL codeSourceLocation = c.getProtectionDomain().getCodeSource().getLocation();
            if (codeSourceLocation != null) {
                return codeSourceLocation;
            }
        } catch (final SecurityException ex) {
            // NB: Cannot access protection domain.
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Cannot access protection domain.", ex);
        } catch (final NullPointerException ex) {
            // NB: Protection domain or code source is null.
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Protection domain or code source is null.", ex);
        }

        // NB: The easy way failed, so we try the hard way. We ask for the class
        // itself as a resource, then strip the class's path from the URL string,
        // leaving the base path.

        // get the class's raw resource path
        final URL classResource = c.getResource(c.getSimpleName() + ".class");
        if (classResource == null) {
            return null; // cannot find class resource
        }

        final String url = classResource.toString();
        final String suffix = c.getCanonicalName().replace('.', '/') + ".class";
        if (!url.endsWith(suffix)) {
            return null; // weird URL
        }

        // strip the class's path from the URL string
        String path = url.substring(0, url.length() - suffix.length());

        // remove the "jar:" prefix and "!/" suffix, if present
        if (path.startsWith("jar:")) {
            path = path.substring(4, path.length() - 2);
        }

        try {
            return new URL(path);
        } catch (final MalformedURLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Converts the given URL string to its corresponding {@link File}.
     *
     * @param url The URL to convert.
     * @return A file path suitable for use with e.g. {@link FileInputStream}
     * @throws IllegalArgumentException if the URL does not correspond to a file.
     */
    public static File urlToFile(final String url) {
        String path = url;
        if (path.startsWith("jar:")) {
            // remove "jar:" prefix and "!/" suffix
            final int index = path.indexOf("!/");
            path = path.substring(4, index);
        }
        try {
            if (path.matches("file:[A-Za-z]:.*")) {
                path = "file:/" + path.substring(5);
            }
            return new File(new URL(path).toURI());
        } catch (final MalformedURLException | URISyntaxException ex) {
            // NB: URL is not completely well-formed.
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "URL is not completely well-formed.", ex);
        }
        if (path.startsWith("file:")) {
            // pass through the URL as-is, minus "file:" prefix
            path = path.substring(5);
            return new File(path);
        }
        throw new IllegalArgumentException("Invalid URL: " + url);
    }
}
