/*
 * Copyright (C) 2015-2023 Ozan Egitmen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.mcotool.client;

import java.util.ArrayList;

public class Database {

    public static final String DB_FILE = com.mcotool.client.Main.WORK_DIR + "database.keys";
    private static final ArrayList<String> data = new ArrayList<>();

    public static void load(String db) {
        try (java.util.stream.Stream<String> stream = java.nio.file.Files.lines(java.nio.file.Paths.get(db))) {
            stream.forEach(data::add);
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(Database.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    public static void set(String keyName, String value) {
        boolean keySet = false;
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).contains(keyName)) {
                if (value == null) {
                    data.remove(i);
                } else {
                    data.set(i, keyName + " " + value);
                }
                keySet = true;
                break;
            }
        }
        if (!keySet && value != null) {
            data.add(keyName + " " + value);
        }
        StringBuilder sb = new StringBuilder();
        data.forEach(key -> sb.append(key).append("\n"));
        try {
            java.nio.file.Files.write(java.nio.file.Paths.get(DB_FILE), sb.toString().getBytes());
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(Database.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    public static void setBool(String keyName, boolean value) {
        set(keyName, String.valueOf(value));
    }

    public static String get(String keyName, String def) {
        String ret = def;
        for (String key : data) {
            if (key.contains(keyName)) {
                ret = key.substring(keyName.length() + 1);
                break;
            }
        }
        return ret;
    }

    public static boolean getBool(String keyName, boolean def) {
        return Boolean.parseBoolean(get(keyName, String.valueOf(def)));
    }
}
